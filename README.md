# Qt 5.12 Hanging bug example

This is meant to be build with QT 5.12.0 and NDK r18b.
Just open the project with qt creator, build it for android and deploy it to some android device.
Le me personally tried Samsung Android-6 and a Motorola One with Android-9.

When you see the list of dummy entries click on the textfield with yellow background.
When the android software keyboard has opened, got to the app chooser menu.
That is on stock android the little square on the right of the android bottom navigation.
You may also go to your home screen, just bring the app in the background for some seconds with opened keyboard.

Now switch back into the app and the programm is unresponsable.

P.S.: If there is a problem like

 error: no such file or directory: '/home/user/Android/android-ndk-r18b/sources/cxx-stl/llvm-libc++/libs/arm64-v8a/libc++.so.16'

Set  ANDROID_NDK_PLATFORM to 28.